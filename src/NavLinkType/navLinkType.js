import React from 'react';
import './nav.css';
import HowItWorks from '../HowItWorks/howItWorks';

const PROMOTE = "promote" 
const SELLER = "seller";

class NavLinkType extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			userType: PROMOTE,
			marketType: 1
		}
		this.markerAdjust.bind(this);
		this.clickUserType.bind(this);
	}

	markerAdjust(e, index){
		
	}

	clickUserType(e, strType){
		
		if(this.state.userType !== strType){
			this.setState({ 
				userType: strType
			});


		}
	}


	//TODO show the different ways of paying.
	//look at Paypal/stripe documentation to see how they do it for integration into JS, HTML, CSS
	//also give the user a means of trying this out. They can do it on a different phone, or a test page, and then it says 'Nice, you got it. Next do it on a real device to get paid'
	render(){
		return(
			<div>
				<div className="n-nav-container">

					<nav className="n-nav g-pos-relative g-flex g-flex-row g-justify-space-around g-mg-bot-3">
						<div></div>
						<div className="n-nav-text" onClick={()=>{ this.markerAdjust(this, 1);} }>Apps</div>
						<div className="n-nav-text">Tweets</div>
						<div className="n-nav-text">Youtube views</div>
						<div className="n-nav-text">Facebook views</div>
						<div></div>
						<div className="g-pos-absolute n-marker"></div>
					</nav>

				</div>
				<div className="g-flex g-flex-row g-justify-space-around">
					<div className={this.state.userType === PROMOTE ? 'active' : ''} onClick={()=>this.clickUserType(this, PROMOTE)}>Promoter</div>
					<div className={this.state.userType === PROMOTE ? '' : 'active'} onClick={()=>this.clickUserType(this, SELLER)}>You have content to promote</div>
				</div>

				<HowItWorks userType={this.state.userType}/>
			</div>
		);
	}
}

export default NavLinkType;