import React from 'react';
import ContentCard from './contentCard';

const TOTAL = "total";
const PERCENT = "percent";

class ContentArea extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			cards: [

			]
		}
	}

	//temporarily add dummy contents in, no internet, no DB
	componentDidMount(){
		for(var i = 0; i < 50; i++){
			this.state.cards.push(
				<ContentCard 
					key={i}
					companyName="Amazon"
					sellingType= {Date.now() % 2 === 0 ? TOTAL : PERCENT}
					commissionPercent={0.24}
					price={0.5}
					numberLeft={275}
					numberTotal={500}
					desc="Super high quality apples for anyone to purchase"
					url="https://www.trademe.co.nz"
				/>
			);
		}
		this.forceUpdate();
	}

	render(){
		return(
			<div className="g-flex g-justify-center g-flex-row g-flex-wrap">
				{
					this.state.cards.map((card, index) => 		
						card
					)
				}
			</div>
		);
	}
}

export default ContentArea;