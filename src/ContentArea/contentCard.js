import React from 'react';
import logo from '../logo.svg';
import './content.css';

class ContentCard extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			companyName: this.props.companyName,
			price: props.price,
			sellingType: props.sellingType,
			numberLeft: props.numberLeft,
			numberTotal: props.numberTotal,
			commissionPercent: props.commissionPercent,
			desc: props.desc,
			url: props.url
		}
	}

	render(){
		return(
			<div className="g-text-center g-mg-3 c-card">
				<div className="g-mg-bot-1">
					<span className="g-mg-left-dot25">{ this.state.companyName }</span>
				</div>

				<div>
				{
					this.state.sellingType === "total" ? 
						//limited by total amount of units advertiser want to sell
						<div>
							<span>{ this.state.numberLeft } sold / { this.state.numberTotal } total at </span>
							<span className="g-mg-right-dot25 c-red"> ${ this.state.price }</span>
							<span>each</span>
						</div>
						:
						//commission by costed amount advertiser wants to sell
						<span>{ this.state.commissionPercent }% of <span className="c-red">$599.99</span></span>
				}
				</div>

				<div>
					<img src={logo} className="App-logo" alt="logo"/>
				</div>

				<div className="g-mg-top-dot5 c-desc">
					{ this.state.desc}
				</div>
				<div className="g-mg-top-1"><a href={this.state.url}>{this.state.url}</a></div>
			</div>
		);
	}
}

export default ContentCard;