import React from 'react';
import './hiw.css';
import logo from '../logo.svg';

class HowItWorks extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			userType: this.props.userType
		}
	}

	render(){
		return(
			<div className="g-text-center">
				{
					this.props.userType === "promote" ?
					//promoter
					<div className="g-flex g-flex-row g-justify-space-around">
						<div className="h-card">
							<strong>1.</strong> Scan a QR code (or a link if you're on a PC)
							<img src={logo} alt="logo"/>
						</div>
						<div className="h-arrow">
							=>
						</div>

						<div className="h-card">
							<strong>2.</strong> Show your link to people
							<img src={logo} alt="logo"/>
						</div>

						<div className="h-arrow">
							=>
						</div>

						<div className="h-card">
							<strong>3. </strong>Get paid everytime someone uses your link and downloads the app
							<img src={logo} alt="logo"/>
						</div>
					</div>
					:
					//seller
					<div className="g-flex g-flex-row g-justify-space-around">
						<div className="h-card">
							<strong>1.</strong> Provide your basic details and a URL link to your content
							<img src={logo} alt="logo"/>
						</div>
						<div className="h-arrow">
							=>
						</div>

						<div className="h-card">
							<strong>2.</strong> Set up the amount of funds that you want to spend
							<img src={logo} alt="logo"/>
						</div>

						<div className="h-arrow">
							=>
						</div>

						<div className="h-card">
							<strong>3.</strong> Decide how much you want to spend per click
							<img src={logo} alt="logo"/>
						</div>

						<div className="h-arrow">
							=>
						</div>

						<div className="h-card">
							<strong>3. </strong>Let users promote your content
							<img src={logo} alt="logo"/>
						</div>
					</div>
				}
			</div>
		);
	}
}

export default HowItWorks;